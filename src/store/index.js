import { createStore } from 'vuex'

export default createStore({
  state: {
    banda:[]
  },
  getters: {
  },
  mutations: {
    setBanda(state, payload){
      state.banda = payload
    }
  },
  actions: {
    async getBanda({commit}, banda){
      //console.log(banda)
      try {
        const res = await fetch(`https://theaudiodb.com/api/v1/json/2/search.php?s=${banda}`)
        const data = await res.json()
        //console.log(data.artists)

        commit('setBanda', data.artists)

      } catch (error) {
        console.log(error)
      }
    }
  },
  modules: {
  }
})
